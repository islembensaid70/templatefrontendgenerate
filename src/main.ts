import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from '@app/app.module';
import {environment} from '@environments/environment';
import {SetDefaultPropService} from "@shared/services/SetDefProps/set-default-prop.service";

if (environment.production) {
  window.console.log = () => {
  };
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));

new SetDefaultPropService()
