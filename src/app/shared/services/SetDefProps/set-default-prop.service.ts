import { Injectable } from '@angular/core';
import {CONFIG} from '@shared/constantes/config';

@Injectable({
  providedIn: 'root'
})
export class SetDefaultPropService {

  constructor() {this.setProps() }

  setProps(){
    (document.querySelector(':root') as HTMLElement).style.setProperty('--primary-color', CONFIG.APP_BASE_COLOR);

  }


}
