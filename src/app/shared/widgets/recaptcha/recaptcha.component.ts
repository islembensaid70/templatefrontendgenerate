import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
    selector: 'st2i-recaptcha',
    templateUrl: './recaptcha.component.html',
    styleUrls: ['./recaptcha.component.scss'],
})
export class RecaptchaComponent implements OnInit {
    @Output() resolve: EventEmitter<string> = new EventEmitter<string>();

    constructor() {
    }

    ngOnInit(): void {
    }

    onResolve(token: string) {
        this.resolve.emit(token);
    }
}
